# Axway `docker-compose` Example

This project contains an example of running the Axway API Management platform in Docker containers orchestrated by `docker-compose`. 

Start:
```
docker-compose up –d
```

Stop:
```
docker-compose down --remove-orphans
```
	
use `docker-compose stop` if you want to keep the containers, then use `docker start {containername}` to restart the specific container.

## Usage

https://adminnodemanager.axway.local:8090/login/
Username: admin
Password: changeme

https://apimanager.axway.local:8075/
Username: apiadmin
Password: changeme

https://apiportal.axway.local/administrator/
Username: admin
Password: changeme

## Deployment Update

After any policy deployment or any environment update the fed is updated.  Run the following steps to save the updated fed.

The following line gets updated with the latest fed that is pushed.  Replace `<updated fed>` with the latest GUID.  

```docker
docker cp apigateway:/opt/Axway/apigateway/groups/emt-group/conf/<updated fed> /Users/enord/Development/gitlab/axwaydocker/compose/data/apigateway/fed/
```

The GUID can be found after a deployment in the Policy Studio Console.  You can see the following line `Successfully loaded configuration 'federated:file:////opt/Axway/apigateway/groups/emt-group/conf/ffadeaac-35fc-4fc0-acdd-b09532fef345/configs.xml'`

Copy this GUID and the new command will look something like this:

```docker
docker cp apigateway:/opt/Axway/apigateway/groups/emt-group/conf/ffadeaac-35fc-4fc0-acdd-b09532fef345 /Users/enord/Development/gitlab/axwaydocker/compose/data/apigateway/fed/
```

Now, that all of the files are added to `data/apigateway/fed/29563aa5-c801-4106-9d93-87bd8fa5b778/ffadeaac-35fc-4fc0-acdd-b09532fef345`.  Now we move the files and remove the old directory.  Make sure you update the new GUID appropriately.

```coffeescript
mv data/apigateway/fed/29563aa5-c801-4106-9d93-87bd8fa5b778/ffadeaac-35fc-4fc0-acdd-b09532fef345/* data/apigateway/fed/29563aa5-c801-4106-9d93-87bd8fa5b778/

rm -rf data/apigateway/fed/29563aa5-c801-4106-9d93-87bd8fa5b778/ffadeaac-35fc-4fc0-acdd-b09532fef345/

sed -i '' 's/\Id: .*/\Id: 29563aa5-c801-4106-9d93-87bd8fa5b778/g' data/apigateway/fed/29563aa5-c801-4106-9d93-87bd8fa5b778/meta-inf/manifest.mf
```

Now when the containers are recreated, everything will be properly deployed.
	
## Setup / Prerequisites

* (optional) increase Docker for OSX RAM to 6 GB
* clone this repo, then `cd docker-example` 
* edit `/etc/hosts` to contain local dns names for containers

```
127.0.0.1 apigateway.axway.local adminnodemanager.axway.local development.axway.local apimanager.axway.local
```

* create a docker network:

```
docker network create -d bridge axway.local
```